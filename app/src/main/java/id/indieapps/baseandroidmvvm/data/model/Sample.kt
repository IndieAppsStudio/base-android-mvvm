package id.indieapps.baseandroidmvvm.data.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created with love by Hari Nugroho on 22/01/2019 at 13.14.
 */

@Entity(tableName = "samples")
data class Sample constructor(
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "number") var number: Long
){
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "_id") var id: Int = 0
}