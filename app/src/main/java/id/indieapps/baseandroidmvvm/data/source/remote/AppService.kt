package id.indieapps.baseandroidmvvm.data.source.remote

import id.indieapps.baseandroidmvvm.util.helper.Network
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created with love by Hari Nugroho on 22/01/2019 at 22.06.
 */
interface AppService {


    companion object Factory {

        fun create(): AppService {

            val mLoggingInterceptor = HttpLoggingInterceptor()
            mLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val mClient = OkHttpClient.Builder()
                .addInterceptor(mLoggingInterceptor)
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build()

            /**
             * Un-comment when unit test
             */
//            val resource = OkHttp3IdlingResource.create("OkHttp", client)
//            IdlingRegistry.getInstance().register(resource)

            val mRetrofit = Retrofit.Builder()
                .baseUrl(Network.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(mClient) //Todo comment if app release
                .build()

            return mRetrofit.create(AppService::class.java)
        }
    }
}