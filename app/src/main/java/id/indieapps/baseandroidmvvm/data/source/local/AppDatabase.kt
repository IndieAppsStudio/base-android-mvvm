package id.indieapps.baseandroidmvvm.data.source.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import id.indieapps.baseandroidmvvm.data.model.Sample
import id.indieapps.baseandroidmvvm.data.source.local.dao.SampleDao

/**
 * Created with love by Hari Nugroho on 21/01/2019 at 22.27.
 */
@Database(entities = [Sample::class], version = 1)
@TypeConverters(DateConverter::class)

abstract class AppDatabase : RoomDatabase() {

    abstract fun sampleDao(): SampleDao

    companion object {

        private var INSTANCE: AppDatabase? = null

        private val lock = Any()

        fun getDatabaseBuilderInstance(context: Context): AppDatabase {
            synchronized(lock) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java, "AppDatabaseName.db")
                        .build()
                }
                return INSTANCE!!
            }
        }

        fun getInMemoryDatabaseInstance(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.inMemoryDatabaseBuilder(context.applicationContext, AppDatabase::class.java)
                    // To simplify the codelab, allow queries on the main thread.
                    // Don't do this on a real app! See PersistenceBasicSample for an example.
                    .allowMainThreadQueries()
                    .build()
            }
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}