package id.indieapps.baseandroidmvvm.data.source.local

import android.arch.persistence.room.TypeConverter
import java.util.*

/**
 * Created with love by Hari Nugroho on 21/01/2019 at 22.30.
 */
class DateConverter {
    @TypeConverter
    fun toDate(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun toTimestamp(date: Date?): Long? {
        return date?.time
    }
}