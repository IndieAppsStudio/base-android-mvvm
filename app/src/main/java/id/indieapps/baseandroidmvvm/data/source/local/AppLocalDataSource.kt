package id.indieapps.baseandroidmvvm.data.source.local

import android.content.SharedPreferences
import android.support.annotation.VisibleForTesting
import id.indieapps.baseandroidmvvm.data.source.AppDataSource
import id.indieapps.baseandroidmvvm.data.source.local.dao.SampleDao
import id.indieapps.baseandroidmvvm.util.AppExecutors

/**
 * Created with love by Hari Nugroho on 21/01/2019 at 22.28.
 */
class AppLocalDataSource private constructor(
    private val appExecutors: AppExecutors,
    private val sampleDao: SampleDao,
    private val preferences: SharedPreferences
) : AppDataSource {



    companion object {
        private var INSTANCE: AppLocalDataSource? = null

        @JvmStatic
        fun getInstance(appExecutors: AppExecutors, sampleDao: SampleDao, preferences: SharedPreferences): AppLocalDataSource {
            if (INSTANCE == null) {
                synchronized(AppLocalDataSource::javaClass) {
                    INSTANCE = AppLocalDataSource(appExecutors, sampleDao, preferences)
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }
}