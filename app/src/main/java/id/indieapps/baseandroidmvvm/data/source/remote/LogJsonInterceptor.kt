package id.indieapps.baseandroidmvvm.data.source.remote

import android.annotation.SuppressLint
import android.util.Log
import id.indieapps.baseandroidmvvm.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import java.io.IOException

/**
 * Created with love by Hari Nugroho on 22/01/2019 at 22.05.
 */
class LogJsonInterceptor : Interceptor {
    @SuppressLint("LongLogTag")
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val response = chain.proceed(request)
        val rawJson = response.body()!!.string()

        Log.d(BuildConfig.APPLICATION_ID, String.format("raw JSON response is: %s", rawJson))

        // Re-create the response before returning it because body can be read only once
        return response.newBuilder()
            .body(ResponseBody.create(response.body()!!.contentType(), rawJson)).build()
    }
}