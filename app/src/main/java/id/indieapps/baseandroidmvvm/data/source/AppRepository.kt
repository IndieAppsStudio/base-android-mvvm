package id.indieapps.baseandroidmvvm.data.source

/**
 * Created with love by Hari Nugroho on 21/01/2019 at 22.31.
 */
class AppRepository(
    val appRemoteDataSource: AppDataSource,
    private val appLocalDataSource: AppDataSource
) : AppDataSource {





    companion object {

        private var INSTANCE: AppRepository? = null

        @JvmStatic
        fun getInstance(mMobilePayslipRemoteDataSource: AppDataSource, mMobilePayslipLocalDataSource: AppDataSource) =
            INSTANCE ?: synchronized(AppRepository::class.java) {
                INSTANCE ?: AppRepository(mMobilePayslipRemoteDataSource, mMobilePayslipLocalDataSource)
                    .also { INSTANCE = it }
            }

        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}