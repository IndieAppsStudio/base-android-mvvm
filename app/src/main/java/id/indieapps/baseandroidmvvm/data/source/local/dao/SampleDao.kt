package id.indieapps.baseandroidmvvm.data.source.local.dao

import android.arch.persistence.room.*
import id.indieapps.baseandroidmvvm.data.model.Sample

/**
 * Created with love by Hari Nugroho on 22/01/2019 at 13.17.
 */

@Dao
interface SampleDao {

    @Query("SELECT * FROM Samples") fun getSamples(): List<Sample>

    @Query("SELECT * FROM Samples WHERE _id = :sampleId") fun getSampleById(sampleId: Int): Sample?

    @Insert(onConflict = OnConflictStrategy.REPLACE) fun insertAccount(sample: Sample)

    @Update
    fun updateAccount(sample: Sample): Int

    @Query("DELETE FROM Samples WHERE _id = :sampleId") fun deleteSampleById(sampleId: Int): Int

    @Query("DELETE FROM Samples") fun deleteSamples()
}