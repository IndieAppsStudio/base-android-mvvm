package id.indieapps.baseandroidmvvm.util

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import id.indieapps.baseandroidmvvm.data.source.AppRepository
import id.indieapps.baseandroidmvvm.mvvm.main.MainViewModel

/**
 * Created with love by Hari Nugroho on 21/01/2019 at 22.37.
 */
class ViewModelFactory private constructor(
    private val mApplication: Application,
    private val mAppRepository: AppRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) =
        with(modelClass) {
            when {
                isAssignableFrom(MainViewModel::class.java) ->
                    MainViewModel(mApplication, mAppRepository)
                else ->
                    throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
            }
        } as T

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile private var INSTANCE: ViewModelFactory? = null

        fun getInstance(mApplication: Application) =
            INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                INSTANCE ?: ViewModelFactory(mApplication,
                    Injection.provideAppRepository(mApplication.applicationContext))
                    .also { INSTANCE = it }
            }
    }
}