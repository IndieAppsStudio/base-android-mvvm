package id.indieapps.baseandroidmvvm.util

import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * Created with love by Hari Nugroho on 21/01/2019 at 22.36.
 */
class DiskIOThreadExecutor : Executor {

    private val diskIO = Executors.newSingleThreadExecutor()

    override fun execute(command: Runnable) { diskIO.execute(command) }
}