package id.indieapps.baseandroidmvvm.util

import android.content.Context
import android.preference.PreferenceManager
import id.indieapps.baseandroidmvvm.data.source.AppRepository
import id.indieapps.baseandroidmvvm.data.source.local.AppDatabase
import id.indieapps.baseandroidmvvm.data.source.local.AppLocalDataSource
import id.indieapps.baseandroidmvvm.data.source.remote.AppRemoteDataSource

/**
 * Created with love by Hari Nugroho on 21/01/2019 at 22.36.
 */
object Injection {

    fun provideAppRepository(context: Context): AppRepository {
        val database = AppDatabase.getDatabaseBuilderInstance(context)
        return AppRepository.getInstance(AppRemoteDataSource,
            AppLocalDataSource.getInstance(AppExecutors(), database.sampleDao(), PreferenceManager.getDefaultSharedPreferences(context)))
    }
}