package id.indieapps.baseandroidmvvm.util.helper

import android.annotation.SuppressLint
import java.text.DateFormat
import java.text.ParsePosition
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created with love by Hari Nugroho on 21/01/2019 at 22.38.
 */
object DateHelper{
    const val SIMPLE_DATE_STRING = "dd-MM-yyyy"
    const val SIMPLE_TIME_STRING = "HH:mm"
    const val SIMPLE_DATE_AND_TIME_STRING = "$SIMPLE_DATE_STRING $SIMPLE_TIME_STRING"

    @JvmField val SIMPLE_DATE_FORMAT = object : ThreadLocal<DateFormat>(){
        @SuppressLint("SimpleDateFormat")
        override fun initialValue(): DateFormat {
            return SimpleDateFormat(SIMPLE_DATE_STRING)
        }
    }

    @JvmField val SIMPLE_TIME_FORMAT = object : ThreadLocal<DateFormat>(){
        @SuppressLint("SimpleDateFormat")
        override fun initialValue(): DateFormat {
            return SimpleDateFormat(SIMPLE_TIME_STRING)
        }
    }

    @JvmField val SIMPLE_DATE_AND_TIME_FORMAT = object : ThreadLocal<DateFormat>(){
        @SuppressLint("SimpleDateFormat")
        override fun initialValue(): DateFormat {
            return SimpleDateFormat(SIMPLE_DATE_AND_TIME_STRING)
        }
    }
}

fun dateAndTimeParse(s: String): Date = DateHelper.SIMPLE_DATE_AND_TIME_FORMAT.get()!!.parse(s, ParsePosition(0))

fun Date.dateAsString(): String = DateHelper.SIMPLE_DATE_FORMAT.get()!!.format(this)

fun Date.timeAsString(): String = DateHelper.SIMPLE_TIME_FORMAT.get()!!.format(this)

fun Long.unixTimeAsString(): String = DateHelper.SIMPLE_DATE_FORMAT.get()!!.format(this * 1000)

fun String.stringToUnixTime(): Long = DateHelper.SIMPLE_DATE_FORMAT.get()!!.parse(this).time/1000

fun currentDate() : String = DateHelper.SIMPLE_DATE_FORMAT.get()!!.format(System.currentTimeMillis())

fun currentTime() : String = DateHelper.SIMPLE_TIME_FORMAT.get()!!.format(System.currentTimeMillis())