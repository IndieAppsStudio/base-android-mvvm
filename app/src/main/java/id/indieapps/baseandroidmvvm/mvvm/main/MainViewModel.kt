package id.indieapps.baseandroidmvvm.mvvm.main

import android.app.Application
import android.databinding.ObservableField
import id.indieapps.baseandroidmvvm.base.BaseViewModel
import id.indieapps.baseandroidmvvm.data.source.AppRepository

/**
 * Created with love by Hari Nugroho on 22/01/2019 at 08.51.
 */
class MainViewModel (
    context: Application,
    appRepository: AppRepository
) : BaseViewModel(context, appRepository){
    var hello = ObservableField<String>()

    fun start() {
        hello.set("Holla")
    }
}