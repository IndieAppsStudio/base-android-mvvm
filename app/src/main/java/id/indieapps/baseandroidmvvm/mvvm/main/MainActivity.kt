package id.indieapps.baseandroidmvvm.mvvm.main

import android.os.Bundle
import id.indieapps.baseandroidmvvm.R
import id.indieapps.baseandroidmvvm.base.BaseActivity
import id.indieapps.baseandroidmvvm.util.obtainViewModel
import id.indieapps.baseandroidmvvm.util.replaceFragmentInActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupFragment()
        subscribeToNavigationChanges()
    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_help_content)
        replaceFragmentInActivity(MainFragment.newInstance(), R.id.frame_help_content)
    }

    private fun subscribeToNavigationChanges() {
    }

    fun obtainViewModel(): MainViewModel = obtainViewModel(MainViewModel::class.java)
}
