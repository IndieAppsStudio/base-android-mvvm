package id.indieapps.baseandroidmvvm.base

/**
 * Created with love by Hari Nugroho on 22/01/2019 at 22.03.
 */
data class BaseApiModel<T> (
    var status: Int = -1,
    var message: String,
    var data: T? = null
)