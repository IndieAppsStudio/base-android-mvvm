package id.indieapps.baseandroidmvvm.base

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import id.indieapps.baseandroidmvvm.data.source.AppRepository

/**
 * Created with love by Hari Nugroho on 21/01/2019 at 22.24.
 */
open class BaseViewModel (
    context: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(context)