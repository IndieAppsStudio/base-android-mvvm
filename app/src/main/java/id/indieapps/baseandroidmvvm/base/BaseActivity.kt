package id.indieapps.baseandroidmvvm.base

import android.content.Context
import android.os.Bundle
import android.support.multidex.MultiDex
import android.support.v7.app.AppCompatActivity

/**
 * Created with love by Hari Nugroho on 21/01/2019 at 22.23.
 */
open class BaseActivity : AppCompatActivity() {

    private lateinit var mActiviy: AppCompatActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mActiviy = this
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}