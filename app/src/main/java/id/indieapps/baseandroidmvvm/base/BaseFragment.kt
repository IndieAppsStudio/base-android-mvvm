package id.indieapps.baseandroidmvvm.base

import android.support.v4.app.Fragment

/**
 * Created with love by Hari Nugroho on 21/01/2019 at 22.24.
 */
open class BaseFragment : Fragment()